#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <wait.h>
#include <string.h>
#include <sqlite3.h>
#include <time.h>


#define REGISTER      1
#define SIGN_IN       2
#define QUERY_WORD    3
#define QUERY_HISTORY 4
#define SIGN_OUT      5

#define OK            1
#define USR_EX        2
#define ERROR         3

typedef struct{
    int  action;
    char usr_name[10];
    char usr_passwd[20];
    char world[20];
}Data_package;

typedef  struct{
    int  statu;
    char world[20];
    char data[1500];
}Ack_package;

pid_t pid;
sqlite3 *db_usr_info;
char history_data_buf[1500] = {0};

void handler(int sig)
{
    wait(NULL);
}


int do_insert_history(sqlite3 *db, Data_package buf);
int do_insert(sqlite3 *db, Data_package buf);
int do_get_table(sqlite3 *db, Data_package buf);
int do_get_table_history(sqlite3 *db, Data_package buf);


int main(int argc, const char *argv[])
{

    //判断程序传入参数是否正常。
    if (argc < 3){
        fprintf(stderr,"请传入两个参数\n");
        exit(1);
    }

    //create database

    if(sqlite3_open("./dictionary.db", &db_usr_info) != SQLITE_OK)
        fprintf(stdout, "error: %s\n", sqlite3_errmsg(db_usr_info));
    else
        fprintf(stdout, "success:The database is created or opened successfully\n");
    
    //create table : usr_info  
    char sql_create_table[128] = "create table usr_info(usr_name char primary key, usr_passwd char)";
    char *errmsg;
    if(sqlite3_exec(db_usr_info, sql_create_table, NULL, NULL, &errmsg) != SQLITE_OK)
        fprintf(stdout, "error:%s\n", errmsg);
    else
        fprintf(stdout, "success:The table is created successfully\n");
    
  

    //创建struct sockaddr_in 结构体
    struct sockaddr_in server_addr;
    struct sockaddr_in client_addr;
    socklen_t client_addr_len = sizeof(client_addr);

    //创建建立链接socket
    int lfd = socket(AF_INET,SOCK_STREAM,0);

    if (-1 == lfd){
        perror("socket lfd");
        exit(1);
    }
    

    //修改server_addr结构体数据
    //将IP和端口号转换为网络字符序。

    in_addr_t addr_int = inet_addr(argv[1]);
    server_addr.sin_addr.s_addr =  addr_int;
    server_addr.sin_port =  htons(atoi(argv[2])); 
    server_addr.sin_family = AF_INET;

    //bind
    if (-1 == bind(lfd,(struct sockaddr *)&server_addr,sizeof(server_addr))){
        perror("bind error");
        exit(1);
    }

    if (-1 ==listen(lfd,100)){
        perror("listen lfd 100");
        exit(1);
    }
    
    int accept_fd;
    signal(SIGCHLD, handler);


    while (1){
        //accept
        accept_fd = accept(lfd,(struct sockaddr *)&client_addr,&client_addr_len);
        if(-1 == accept_fd){
            perror("accept error");
            exit(1);
        }
        
        pid = fork();

        if(pid > 0){
            
        }else if(0 == pid){

            Data_package buf_recv;
            Ack_package  buf_send;
            int ret;
            FILE *dict_fd;
            char buf_row_data[300];
            char buf_word[20];
            char buf_data[300];
            int data_len;
            
            //open dict.txt
            dict_fd = fopen("./dict.txt", "r");
            if(NULL == dict_fd){
                perror("fopen error");
                exit(1);
            }

            while(1){

                //recv Data_package form client
                memset(&buf_recv, 0, sizeof(buf_recv));
                recv(accept_fd, &buf_recv, sizeof(buf_recv), 0);

                switch(buf_recv.action){

                case REGISTER:

                    memset(&buf_send, 0, sizeof(buf_send));
                    ret = do_insert(db_usr_info, buf_recv);
                    if(ret == 0){
                        fprintf(stdout, "insert successfull\n");
                        buf_send.statu = OK;
                        send(accept_fd, &buf_send, sizeof(buf_send), 0);
                    }else if(ret == 19){
                        fprintf(stdout, "USR_EX\n");
                        buf_send.statu = USR_EX;
                        send(accept_fd, &buf_send, sizeof(buf_send), 0);
                    }
                    break;

                case SIGN_IN:

                    memset(&buf_send, 0, sizeof(buf_send));
                    ret = do_get_table(db_usr_info, buf_recv);
                    if(ret == OK){
                        fprintf(stdout, "sign in successfull\n");
                        buf_send.statu = OK;
                        send(accept_fd, &buf_send, sizeof(buf_send), 0);
                    }else if(ret == ERROR){
                        fprintf(stdout, "ERROR\n");
                        buf_send.statu = ERROR;
                        send(accept_fd, &buf_send, sizeof(buf_send), 0);
                    }
                    break;
                case QUERY_WORD:
            
                    do_insert_history(db_usr_info, buf_recv);
                    //rewind dict_fd
                    rewind(dict_fd);
                    //get row data
                    while(1){
                        memset(buf_row_data, 0, sizeof(buf_row_data));
                        
                        if(NULL == fgets(buf_row_data, sizeof(buf_row_data), dict_fd)){
                            //dict in the end
                            
                            fprintf(stdout, "no the word\n");

                            memset(&buf_send, 0, sizeof(buf_send));
                            buf_send.statu = ERROR;
                            strcpy(buf_send.world, buf_recv.world);
                            send(accept_fd, &buf_send, sizeof(buf_send), 0);

                            break;
                        }
                        //fprintf(stdout, "%s\n", buf_row_data);
                        buf_row_data[strlen(buf_row_data) - 1] = '\0';
                
                        //get word
                        memset(buf_word, 0, sizeof(buf_word));
                        memset(buf_data, 0, sizeof(buf_data));
                        sscanf(buf_row_data,"%s", buf_word);
                        data_len = strlen(buf_word);

                        fprintf(stdout, "buf_word =  %s\n", buf_word);
                        strcpy(buf_data, (buf_row_data+data_len));
                        fprintf(stdout, "buf_data = %s\n", buf_data);
                        //fprintf(stdout, "%s\n", buf_data);

                        if(0 == strcmp(buf_word, buf_recv.world)){

                            fprintf(stdout, "get the word\n");
                            //set buf_send var
                            memset(&buf_send, 0, sizeof(buf_send));
                            buf_send.statu = OK;
                            strcpy(buf_send.world, buf_recv.world);
                            strcpy(buf_send.data, buf_data);
                            send(accept_fd, &buf_send, sizeof(buf_send), 0);

                            break;
                        }
                    }
                    
                    break;
                case QUERY_HISTORY:

                    fprintf(stdout, "QUERY_HISTORY\n");
                    memset(&buf_send, 0, sizeof(buf_send));
                    buf_send.statu = OK;
                    /////////function/////////get the data//////
                    //buf_send.data ///////
                    do_get_table_history(db_usr_info, buf_recv);

                    strcpy(buf_send.data, history_data_buf);
                    fprintf(stdout, "---------------\n");
                    send(accept_fd, &buf_send, sizeof(buf_send), 0);

                    break;
                case SIGN_OUT:
                    //exit pid
                    exit(1);
                    break;
                }
            }
        }
    }

    return 0;
}

int do_get_table_history(sqlite3 *db, Data_package buf) 
{
    char **ret;
    int rows, columns;
    char *errmsg;
    char cmd[128] = {0};
    int i, j, n = 2;
    //clear history_data_buf
    memset(history_data_buf, 0, sizeof(history_data_buf));


    sprintf(cmd, "select * from usr_%s", buf.usr_name);
    if(sqlite3_get_table(db, cmd, &ret, &rows, &columns, &errmsg) != SQLITE_OK){
        fprintf(stdout, "error:%s\n", errmsg);
    }
    if(rows <=  0){
        return ERROR;
    }else if(rows > 0){

        for(i = 0; i < rows; i++){
            for(j = 0; j < columns; j++){
                if((n%2) == 0){
                    strcat(history_data_buf, ret[n]);
                    strcat(history_data_buf, "------>");
                }else{
                    strcat(history_data_buf, ret[n]);
                    strcat(history_data_buf, "\n");
                }
                printf("ret[%d] = %s\n", n, ret[n]);
                n++;
            }
        }

        return OK;
    }

}

int do_insert_history(sqlite3 *db, Data_package buf)
{   
    char cmd[128] = {0};
    char *errmsg;
    int errcode;
    time_t now;
    char buf_time[30];

    now = time(NULL);
    sprintf(buf_time, "%s", ctime(&now));
    buf_time[strlen(buf_time) - 1] = '\0';

    sprintf(cmd, "insert into usr_%s values('%s', '%s')", buf.usr_name, buf_time, buf.world);

    if(SQLITE_OK == (errcode = sqlite3_exec(db, cmd, NULL, NULL, &errmsg))){
        return 0;
    }else{
        fprintf(stdout, "errcode = %d\n", errcode);
        return errcode;
    }
}

int do_insert(sqlite3 *db, Data_package buf)
{   
    char cmd[128] = {0};
    char *errmsg;
    int errcode;

    sprintf(cmd, "insert into usr_info values('%s', '%s')", buf.usr_name, buf.usr_passwd);

    if(SQLITE_OK == (errcode = sqlite3_exec(db, cmd, NULL, NULL, &errmsg))){
        
        fprintf(stdout, "insert successfull\n");
        fprintf(stdout, "create usr_%s table\n", buf.usr_name);
        //create usr_table
        char sql_create_table[128]; 
        sprintf(sql_create_table, "create table usr_%s(time char, word char)", buf.usr_name);
        char *errmsg;
        if(sqlite3_exec(db_usr_info, sql_create_table, NULL, NULL, &errmsg) != SQLITE_OK){
            fprintf(stdout, "error:%s\n", errmsg);
        }else{
            fprintf(stdout, "success:The table is created successfully\n");
        }

        //insert successfull;
        return 0;
    }else{
        fprintf(stdout, "errcode = %d\n", errcode);
        return errcode;
    }
}

int do_get_table(sqlite3 *db, Data_package buf)
{
    char **ret;
    int rows, columns;
    char *errmsg;
    char cmd[128] = {0};
    sprintf(cmd, "select * from usr_info where usr_name = '%s' and usr_passwd = '%s'", buf.usr_name, buf.usr_passwd);
    if(sqlite3_get_table(db, cmd, &ret, &rows, &columns, &errmsg) != SQLITE_OK){
        fprintf(stdout, "error:%s\n", errmsg);
    }
    if(rows <=  0){
        return ERROR;
    }else if(rows > 0){
        return OK;
    }
}


