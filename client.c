#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>

#define REGISTER      1
#define SIGN_IN       2
#define QUERY_WORD    3
#define QUERY_HISTORY 4
#define SIGN_OUT      5

#define OK            1
#define USR_EX        2
#define ERROR         3

typedef struct{
    int  action;
    char usr_name[10];
    char usr_passwd[20];
    char world[20];
}Data_package;

typedef  struct{
    int  statu;
    char world[20];
    char data[1500];
}Ack_package;


int main(int argc, const char *argv[])
{
    if(argc < 3){
        fprintf(stderr,"请输入正确的参数\n");
        exit(1);
    }

    int lfd = socket(AF_INET,SOCK_STREAM,0);
    if(-1 == lfd){
        perror("socket error");
        exit(1);
    }

    //struct sockaddr_in client_addr;
    struct sockaddr_in server_addr;
    
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(atoi(argv[2]));
    server_addr.sin_addr.s_addr = inet_addr(argv[1]);

    

    if(-1 == connect(lfd,(struct sockaddr*)&server_addr,sizeof(server_addr))){
        perror("connect error");
        exit(1);
    }

    Data_package buf_send;
    Ack_package  buf_recv;
    char  flag_interface_1 = 0;
    char flag_interface_2 = 0;
    int ret;

NEXT:
    while(1){

        //interface 1
        fprintf(stdout, "[-----------------------------------------------]\n");
        fprintf(stdout, "[------[1.Register]  [2.Sign_in]  [3.Quit]------]\n");
        fprintf(stdout, "[-----------------------------------------------]\n");

        scanf("%c", &flag_interface_1);
        getchar();

        switch(flag_interface_1){

        //1. register
        case '1':

            while(1){
                memset(&buf_send, 0, sizeof(buf_send));
                // set buf_send var
                buf_send.action = REGISTER;
                fprintf(stdout, "[Please Input:]\n"); 
                fprintf(stdout, "[Usr Name >");

                fgets(buf_send.usr_name, sizeof(buf_send.usr_name), stdin);
                buf_send.usr_name[strlen(buf_send.usr_name) - 1] = 0;

                if(0 == strcmp(buf_send.usr_name, "#")){
                    break;
                }

                fprintf(stdout, "[Usr Passwd >>");
                fgets(buf_send.usr_passwd, sizeof(buf_send.usr_passwd), stdin);
                buf_send.usr_passwd[strlen(buf_send.usr_passwd) - 1] = 0;

                //send to server
                send(lfd, &buf_send, sizeof(buf_send), 0); 

                //recv from server
                memset(&buf_recv, 0, sizeof(buf_recv));
                recv(lfd, &buf_recv, sizeof(buf_recv), 0);
                
                if(buf_recv.statu ==  OK){
                    fprintf(stdout, "[Register Successfull!!]\n");
                    break; 
                }else if(buf_recv.statu == USR_EX){
                    fprintf(stdout, "[Usr Name Existence!]\n");
                    continue;
                }
            }
            break;
        
        //2.sign_in
        case '2':

            while(1){

                memset(&buf_send, 0, sizeof(buf_send));
                // set buf_send var
                buf_send.action = SIGN_IN;
                fprintf(stdout, "[Please Input:]\n"); 
                fprintf(stdout, "[Usr Name >");


                fgets(buf_send.usr_name, sizeof(buf_send.usr_name), stdin);
                buf_send.usr_name[strlen(buf_send.usr_name) - 1] = 0;

                if(0 == strcmp(buf_send.usr_name, "#")){
                    break;
                }

                fprintf(stdout, "[Usr Passwd >>");
                fgets(buf_send.usr_passwd, sizeof(buf_send.usr_passwd), stdin);
                buf_send.usr_passwd[strlen(buf_send.usr_passwd) - 1] = 0;

                //send to server
                send(lfd, &buf_send, sizeof(buf_send), 0); 

                //recv from server
                memset(&buf_recv, 0, sizeof(buf_recv));
                recv(lfd, &buf_recv, sizeof(buf_recv), 0);
                
                if(buf_recv.statu ==  OK){
                    fprintf(stdout, "[Sign In Successfull!!]\n");

                    //interface 2
                    // quest : 1, quit break;
                    // quest : 2, name and passwd var dont change
                    while(1){

                        //clear passwd
                        memset(buf_send.usr_passwd, 0, sizeof(buf_send.usr_passwd));
                        fprintf(stdout, "[-----------------------------------------------]\n");
                        fprintf(stdout, "[---[1.Query Word] [2.Query History] [3.Quit]---]\n");
                        fprintf(stdout, "[-----------------------------------------------]\n");

                        flag_interface_2 = 0;
                        scanf("%c", &flag_interface_2);
                        getchar();

                        switch(flag_interface_2){
                        case '1':

                            while(1){
                                //set buf_send var
                                memset(buf_send.world, 0, sizeof(buf_send.world));
                                buf_send.action = QUERY_WORD;
                                fprintf(stdout, "[ Please Input A Word Witch You Want To Query ]\n");
                                fprintf(stdout, "> ");
                                fgets(buf_send.world, sizeof(buf_send.world), stdin);
                                buf_send.world[strlen(buf_send.world) - 1] = '\0';

                                if(0 == strcmp(buf_send.world, "#")){
                                    break;
                                }

                                send(lfd, &buf_send, sizeof(buf_send), 0);

                                memset(&buf_recv, 0, sizeof(buf_recv));
                                recv(lfd, &buf_recv, sizeof(buf_recv), 0);

                                if(buf_recv.statu == OK){
                                    fprintf(stdout, "----------Query Results :---------- \n");
                                    fprintf(stdout, "%s \n", buf_recv.data);
                                    fprintf(stdout, "--------Query Results End---------- \n");
                                }else if(buf_recv.statu == ERROR){
                                    fprintf(stdout, "[ There Is No Such Word ]\n");
                                    continue;
                                }
                            }

                            break;
                        case '2':

                            memset(buf_send.world, 0, sizeof(buf_send.world));
                            buf_send.action = QUERY_HISTORY;
                            send(lfd, &buf_send, sizeof(buf_send), 0);

                            memset(&buf_recv, 0, sizeof(buf_recv));
                            recv(lfd, &buf_recv, sizeof(buf_recv), 0);

                            fprintf(stdout, "-----------Query Results :-----------\n");
                            fprintf(stdout, "%s\n", buf_recv.data);
                            fprintf(stdout, "---------Query Results End-----------\n");

                            break;
                        case '3':
                            goto NEXT;
                            break;
                        default:
                            fprintf(stdout, "[Input ERROR]\n");
                            break;
                        }

                    }
                }else if(buf_recv.statu == ERROR){
                    fprintf(stdout, "[Usr Name Or Passwd Error!]\n");
                    continue;
                }

            }
            break;

        //3.quit
        case '3':

            //sent server quit
            memset(&buf_send, 0, sizeof(buf_send));
            buf_send.action = SIGN_OUT;
            send(lfd, &buf_send, sizeof(buf_send), 0);
            fprintf(stdout, "[APP QUIT]\n");
            exit(1);
            break;
        default:
            fprintf(stdout, "[Input ERROR]\n");
            break;
        }
        







#if 0
        fgets(buf,sizeof(buf),stdin);        
        buf[strlen(buf) - 1] = 0;
        if(strcmp(buf,"quit") == 0){
            exit(1);
        }

        write(lfd,buf,strlen(buf));

        memset(buf,0,sizeof(buf));
        if(0 == (ret = read(lfd,buf,sizeof(buf)))){
            fprintf(stdout, "server sign out\n");
            fprintf(stdout, "app quit \n");
            exit(1);
        }
        fprintf(stdout,"%s\n",buf);
        memset(buf,0,sizeof(buf));
#endif
    }


    return 0;
}

