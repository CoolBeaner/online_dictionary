all:server.o client.o
	gcc server.o -o server -lsqlite3
	gcc client.o -o client
	rm *.o
server.o:
	gcc -c server.c -o server.o -lsqlite3
client.o:
	gcc -c client.c -o client.o
clean:
	rm server client
